package com.viking.web.controller;

import java.net.URI;

import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entities.Panier;
import com.viking.service.NoArticleInStockException;
import com.viking.service.PanierService;

//@CrossOrigin(origins = {"http://localhost:4200"}, allowCredentials = "true")
@RestController
@RequestMapping(path="/panier")
public class PanierController {
	
	private PanierService panierService;

	public PanierController(PanierService panierService) {
		super();
		this.panierService = panierService;
	}
	
	@GetMapping("{idpanier}")
	public Panier getPanierById(Long idPanier) {
		return panierService.getPanierById(idPanier);
	}
	
	@PostMapping("{id}")
	public ResponseEntity<Panier> validerPanier(@PathVariable Long id, UriComponentsBuilder uriComponentsBuilder) throws NoArticleInStockException {
		
		Panier panier = panierService.validerPanier(id);
		URI uri = uriComponentsBuilder.path("/categorie/{id}").buildAndExpand(id).toUri();
		return ResponseEntity.created(uri).body(panier);
		
	}
	
}
