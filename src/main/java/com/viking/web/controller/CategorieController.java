package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.entities.Categorie;
import com.viking.service.CategorieNotFoundException;
import com.viking.service.CategorieService;


//@CrossOrigin(origins = {"http://localhost:4200"}, allowCredentials = "true")
@RestController
@RequestMapping(path="/categorie")
public class CategorieController {
	
	private CategorieService categorieService;
	
	@ExceptionHandler(CategorieNotFoundException.class)
	@ResponseStatus(code = HttpStatus.NOT_FOUND)
	public String handle(CategorieNotFoundException e) {
		return e.getMessage();
	}
	
	public CategorieController(CategorieService categorieService) {
		super();
		this.categorieService = categorieService;
	}

	@GetMapping("{idCategorie}")
	public Categorie getCategorieById(@PathVariable long idCategorie) throws CategorieNotFoundException {
			return categorieService.getCategorieById(idCategorie);
	}
	
	@GetMapping
	public List<Categorie> getAllCategorie() {
		return categorieService.getAllCategorie();
	}
	
	@DeleteMapping("{idCategorie}")
	public void deleteCategorie(@PathVariable long idCategorie) throws CategorieNotFoundException {
		categorieService.deleteCategorie(idCategorie);
	}
	
	@PutMapping("{idCategorie}")
	public Categorie update(@PathVariable long idCategorie, @RequestBody Categorie newCategorie) throws CategorieNotFoundException {
		newCategorie.setId(idCategorie);
		categorieService.update(newCategorie);
		return newCategorie;
	}
	
	@PostMapping
	public ResponseEntity<Categorie> createCategorie(@RequestBody Categorie newCategorie, UriComponentsBuilder uriComponentsBuilder) {
		categorieService.createCategorie(newCategorie);
		Long id = newCategorie.getId();
		URI uri = uriComponentsBuilder.path("/categorie/{id}").buildAndExpand(id).toUri();
		
		return ResponseEntity.created(uri).body(newCategorie);
	}
	
}
