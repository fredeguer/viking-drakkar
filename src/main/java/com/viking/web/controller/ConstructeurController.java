package com.viking.web.controller;

import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.viking.dao.ConstructeurDao;	
import com.viking.entities.Constructeur;

@RestController
@RequestMapping(path="/constructeur")
public class ConstructeurController {

	private ConstructeurDao constructeurDao;

	public ConstructeurController(ConstructeurDao constructeurDao) {
		super();
		this.constructeurDao = constructeurDao;
	}
	
	@GetMapping("{idCategorie}")
	public Constructeur getConstructeurById(@PathVariable long idConstructeur) {
			return constructeurDao.getConstructeurById(idConstructeur);
	}
	
	@GetMapping
	public List<Constructeur> getAllConstructeur() {
		return constructeurDao.getAllConstructeur();
	}
}
