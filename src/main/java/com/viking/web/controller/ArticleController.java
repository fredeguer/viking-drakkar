package com.viking.web.controller;

import java.net.URI;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
//import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.UriComponentsBuilder;

import com.viking.dao.ArticleDao;
import com.viking.entities.Article;
import com.viking.entities.Categorie;

//@CrossOrigin(origins = {"http://localhost:4200"}, allowCredentials = "true")
@RestController
@RequestMapping(path="/article")
public class ArticleController {
	
	@Autowired
	private ArticleDao articleDao;
	
	@GetMapping("{idArticle}")
	public Article getArticleById(@PathVariable Long idArticle) {
		return articleDao.getArticleById(idArticle);
	}
	
	@GetMapping
	public List<Article> getAllArticle() {
		return articleDao.getAllArticle();
	}
	
	@PostMapping
	public ResponseEntity<Article> createArticle(@RequestBody Article article, UriComponentsBuilder uriComponentsBuilder) {
		articleDao.createArticle(article);
		Long id = article.getId();
		URI uri = uriComponentsBuilder.path("/article/{id}").buildAndExpand(id).toUri();
		return ResponseEntity.created(uri).body(article);
	}

}
