package com.viking.web.controller;

public class RechercheParCategorie {
	
	private String nomCategorie;

	public String getNomCategorie() {
		return nomCategorie;
	}

	public void setNomCategorie(String nomCategorie) {
		this.nomCategorie = nomCategorie;
	}

}
