package com.viking.web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class WelcomeController {

		@GetMapping("/welcome")
		public String welcome(Model model) {
			RechercheParCategorie rechercheParCategorie = new RechercheParCategorie();
			rechercheParCategorie.setNomCategorie("yaourt");
			model.addAttribute("rechercheParCategorie", rechercheParCategorie);
			return "welcome";
		}
		
		@PostMapping("/welcome")
		public String rechercher(RechercheParCategorie rechercheParCategorie) {
			
			System.out.println(">>>>>>>>>>>" + rechercheParCategorie.getNomCategorie());
			return "welcome";
			
		}
}
