package com.viking.web.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloController {
	
	@GetMapping("/hello")
	public String get(@RequestParam(name="nom", required=false) String name) {
		return "hello " + name;
	}
	
	
}
